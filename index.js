/**Инкапсуляция */

// описываем класс
function Book() {
  // Name переменная недоступная извне
  var Name = "";

  // Price переменная доступная извне
  this.Price = "";

  this.setName = function (name) {
    Name = name;
  };

  this.getName = function () {
    return Name;
  };
}

// создаем экземпляр класса
var book1 = new Book();

// Price
book1.Price = 10;
alert(book1.Price); // на экране увидим 10

// set Name
book1.setName("Волшебник Средиземноморья");
alert(book1.getName()); // на экране увидим "Волшебник Средиземноморья"

// Name
alert(book1.Name); // Error! book1.Name is undefined

/**Наследование */

function Machine() {
  var enabled = false;

  this.enable = function () {
    enabled = true;
  };

  this.disable = function () {
    enabled = false;
  };
}

// публичное свойство waterAmount и приватное свойство power.
function CoffeeMachine(power) {
  this.waterAmount = 0;
  console.log("Created coffee machine with: " + power + " watts");

  // расчёт времени для кипячения
  function getBoilTime() {
    return 1000;
  }

  // что делать по окончании процесса
  function onReady() {
    alert("Кофе готов!");
  }

  this.run = function () {
    // setTimeout - встроенная функция,
    // она запустит onReady через getBoilTime() миллисекунд
    setTimeout(onReady, getBoilTime());
  };
}

var coffeeMachine = new CoffeeMachine(100);
coffeeMachine.waterAmount = 200;
coffeeMachine.run();

var powerfulCoffeeMachine = new CoffeeMachine(1000);
powerfulCoffeeMachine.waterAmount = 300;

/**Полиморфизм 
 * Переопределение методов – это тип полиморфизма во время выполнения. 
 * Например, используется для переопределения методов родительского класса 
 * в дочернем классе. 
*/
var makeDrinkForPerson = function (drinkMachine, person) {
  drinkMachine.waterAmount = 200;
  var drink = drinkMachine.run();
  person.give(drink);
};
var cocktailMaker = new CocktailMaker('rum', 'cola');
var person = findPersonByName('John');
makeDrinkForPerson(cocktailMaker, person);